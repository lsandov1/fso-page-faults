#include <stdio.h>
#include <stdlib.h>

#ifdef DEBUG
#define N 1<<2 // 2^2=4
#define M 1<<2  // 2^2=4
#else
#define N 1<<12 // 2^12=4096
#define M 1<<10  // 2^10=1024
#endif

int m[N][M];

int main(int argc, char *argv[]) {

  char order;
  
  // get the order from command line, defaults to row-order
  switch (argc)
    {
    case 1: // no argumetns
      order='r';
      break;
    case 2: // one argument
      order=argv[1][0];
      break;
    default: // two or more arguments are not permitted
      fprintf(stderr,"%s [r|c]", argv[0]);
      exit(0);
    }
  
  printf("Matrix Shape %d %d\n", N, M);

  if (order == 'r')
    {
      for(int i=0; i<N; i++) {
	for(int j=0;j<M; j++) {
	  m[i][j] = i*j;
	  #ifdef DEBUG
	  printf("m[%d][%d] = %p\n", i,j,&m[i][j]);
	  #endif
	}
      }
    }
  else
    {
      for(int j=0;j<M; j++) {
	for(int i=0;i<N; i++) {
	  m[i][j] = i*j;
	  #ifdef DEBUG
	  printf("m[%d][%d] = %p\n", i,j,&m[i][j]);
	  #endif
	}
      }
    }

  exit(0);
}
