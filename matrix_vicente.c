#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

void populateRowOrder(int **matrix, int rows, int cols) {
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
	  matrix[i][j] = i*j;//rand() % 100;
        }
    }
}
 
void populateColumnOrder(int **matrix, int rows, int cols) {
    for (int j = 0; j < cols; j++) {
        for (int i = 0; i < rows; i++) {
	  matrix[i][j] = i*j;//rand() % 100; 
        }
    }
}
 
void printMatrix(int **matrix, int rows, int cols) {
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            printf("%4d ", matrix[i][j]);
        }
        printf("\n");
    }
}
 
int main(int argc, char *argv[]) {
    if (argc != 2) {
        printf("Usage: %s <row|column>\n", argv[0]);
        return 1;
    }
 
    srand(time(NULL)); // Seed the random number generator
 
    int rows = 1<<12; // Number of rows
    int cols = 1<<10; // Number of columns
 
    int **matrix = (int **)malloc(rows * sizeof(int *));
    for (int i = 0; i < rows; i++) {
        matrix[i] = (int *)malloc(cols * sizeof(int));
    }
 
    if (matrix == NULL) {
        printf("Memory allocation failed.\n");
        return 1;
    }
 
    if (strcmp(argv[1], "row") == 0) {
        populateRowOrder(matrix, rows, cols);
    } else if (strcmp(argv[1], "column") == 0) {
        populateColumnOrder(matrix, rows, cols);
    } else {
        printf("Invalid parameter. Use 'row' or 'column'.\n");
        return 1;
    }
 
    //printf("Generated matrix in %s order:\n", argv[1]);
    //printMatrix(matrix, rows, cols);
 
    // Free allocated memory
    for (int i = 0; i < rows; i++) {
        free(matrix[i]);
    }
    free(matrix);
 
    return 0;
}
