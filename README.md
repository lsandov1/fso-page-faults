- What is a page fault?

A program tries to read/write to an address that is at a page that is not (yet) present at memory
then the OS brings it from secundary to primary memory; this task carried by the OS on behalf of
a process is call a **page fault** and degrades performance.

- What is the size of a page on your Linux OS?

```
$ getconf PAGESIZE
4096
```

- How is an matrix stored in the memory? row-order or column order?

**row-order**

- How defines the order? language ? compiler? OS?

The language 

- Which order causes more faults? why?

Both cause the same faults so it does matter if we iterate row or column order, we get the same
page faults in both. However, it is considerable faster transversing the matrix in row-order and the
reason is explaned at this [wiki page](https://en.wikipedia.org/wiki/Row-_and_column-major_order).

Profiling results:

Non-Optimize code


```
make profile
gcc  -o matrix matrix.c -lm
sudo perf stat -r 1 ./matrix r
Matrix Shape 4096 1024

 Performance counter stats for './matrix r':

             11.40 msec task-clock                #    0.977 CPUs utilized          
                 0      context-switches          #    0.000 /sec                   
                 0      cpu-migrations            #    0.000 /sec                   
             4 151      page-faults               #  364.170 K/sec                  
        48 062 086      cycles                    #    4.217 GHz                    
        75 951 105      instructions              #    1.58  insn per cycle         
         7 274 243      branches                  #  638.174 M/sec                  
             9 485      branch-misses             #    0.13% of all branches        

       0.011664430 seconds time elapsed

       0.007795000 seconds user
       0.003897000 seconds sys


sudo perf stat -r 1 ./matrix c
Matrix Shape 4096 1024

 Performance counter stats for './matrix c':

             57.19 msec task-clock                #    0.997 CPUs utilized          
                 2      context-switches          #   34.972 /sec                   
                 0      cpu-migrations            #    0.000 /sec                   
             4 148      page-faults               #   72.533 K/sec                  
       251 309 028      cycles                    #    4.394 GHz                    
        76 939 681      instructions              #    0.31  insn per cycle         
         7 403 089      branches                  #  129.452 M/sec                  
            12 171      branch-misses             #    0.16% of all branches        

       0.057376302 seconds time elapsed

       0.053274000 seconds user
       0.004098000 seconds sys
```

Optimize code

```
gcc  -O -o matrix matrix.c -lm
sudo perf stat -r 1 ./matrix r
Matrix Shape 4096 1024

 Performance counter stats for './matrix r':

              5.11 msec task-clock                #    0.963 CPUs utilized          
                 0      context-switches          #    0.000 /sec                   
                 0      cpu-migrations            #    0.000 /sec                   
             4 150      page-faults               #  812.738 K/sec                  
        22 910 022      cycles                    #    4.487 GHz                    
        38 738 962      instructions              #    1.69  insn per cycle         
         7 342 846      branches                  #    1.438 G/sec                  
             9 825      branch-misses             #    0.13% of all branches        

       0.005304619 seconds time elapsed

       0.000000000 seconds user
       0.005332000 seconds sys


sudo perf stat -r 1 ./matrix c
Matrix Shape 4096 1024

 Performance counter stats for './matrix c':

             35.97 msec task-clock                #    0.993 CPUs utilized          
                 0      context-switches          #    0.000 /sec                   
                 0      cpu-migrations            #    0.000 /sec                   
             4 150      page-faults               #  115.384 K/sec                  
       161 459 722      cycles                    #    4.489 GHz                    
        38 677 926      instructions              #    0.24  insn per cycle         
         7 333 989      branches                  #  203.910 M/sec                  
            11 272      branch-misses             #    0.15% of all branches        

       0.036219824 seconds time elapsed

       0.032215000 seconds user
       0.004026000 seconds sys
```
